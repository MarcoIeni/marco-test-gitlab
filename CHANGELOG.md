# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.5](https://gitlab.com/MarcoIeni/marco-test-gitlab/compare/v0.1.4...v0.1.5) - 2024-06-21

### Other
- wip

## [0.1.4](https://gitlab.com/MarcoIeni/marco-test-gitlab/compare/v0.1.3...v0.1.4) - 2024-06-21

### Other
- wip

## [0.1.3](https://gitlab.com/MarcoIeni/marco-test-gitlab/compare/v0.1.2...v0.1.3) - 2024-06-18

### Other
- wip

## [0.1.2](https://gitlab.com/MarcoIeni/marco-test-gitlab/compare/v0.1.1...v0.1.2) - 2024-06-18

### Other
- test

## [0.1.1](https://gitlab.com/MarcoIeni/marco-test-gitlab/releases/tag/v0.1.1) - 2023-08-15

### Other
- rename crate
- helloo
- hello
- changelog

## [0.1.0](https://gitlab.com/MarcoIeni/marco-test-gitlab/releases/tag/v0.1.0) - 2023-03-18

### Other
- Initial commit
